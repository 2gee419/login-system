const express = require("express");
const path = require('path');
const mysql = require("mysql");
const dotenv = require('dotenv');
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { promisify } = require('util');
const multer = require('multer');
const bodyparser = require('body-parser');



dotenv.config({ path: './.env'});

const app = express();

const db = mysql.createConnection({
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE
});

const publicDirectory = path.join(__dirname, './public');
app.use(express.static(publicDirectory));

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded({ extended: false }));
// Parse JSON bodies (as sent by API clients)
app.use(express.json());
app.use(cookieParser());

app.set('view engine', 'hbs');

db.connect( (error) => {
  if(error) {
    console.log(error)
  } else {
    console.log("MYSQL Connected...")
  }
})

// //! Use of Multer
// var storage = multer.diskStorage({
//       destination: (req, file, callBack) => {
//           callBack(null, './public/')     // './public/images/' directory name where save the file
//       },
//       filename: (req, file, callBack) => {
//           callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
//       }
//   })
//    
//   var upload = multer({
//       storage: storage
// });

//Define Routes
app.use('/', require('./routes/pages'));
app.use('/auth', require('./routes/auth'));

// //@type   POST
// //route for post data
// app.post("/auth/update-img", upload.single('image'), async(req, res) => {
//            
//   if( req.cookies.jwt) {
//     try {
//       //1) verify the token
//       const decoded = await promisify(jwt.verify)(req.cookies.jwt,
//       process.env.JWT_SECRET
//       );

//       console.log(decoded);

//       //2) Check if the user still exists
//       db.query('SELECT * FROM users WHERE id = ?', [decoded.id], (error, result) => {
//         console.log(result);

//         if(!result) {
//           return next();
//         }

//         if (!req.file) {
//               req.user = result[0];
//               console.log(req.user);
//               return res.render('profile', {
//                 user: req.user,
//                 message2: 'no image to upload'
//               });
//         }

//         req.user = result[0];
//         console.log(req.file.filename)
//         const imgsrc = 'http://127.0.0.1:5001/' + req.file.filename

//         if(result.length > 0) {
//           db.query('UPDATE users SET img = ? WHERE email = ?',[imgsrc,req.user.email], (err, result) => {
//             if (err) throw err;
//             return res.redirect('/profile');
//           })
//         }
//       });
//     } catch (error) {
//       console.log(error);
//       return next();
//     }
//   } else {
//     next();
//   }
// });

app.listen(5001, () => {
  console.log("Server started on Port 5001");
})
const express = require('express');
const multer = require('multer');
const path = require('path');
const authController = require('../controllers/auth');

//! Use of Multer
var storage = multer.diskStorage({
        destination: (req, file, callBack) => {
            callBack(null, './public/')     // './public/images/' directory name where save the file
        },
        filename: (req, file, callBack) => {
            callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
        }
    })
     
    var upload = multer({
        storage: storage
});

const router = express.Router();
          
router.post('/register', authController.register );

router.post('/login', authController.login );

router.post('/update-password', authController.password);

router.post('/update-img', upload.single('image') , authController.image);

router.get('/logout', authController.logout );


module.exports = router;
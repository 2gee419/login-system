const mysql = require("mysql");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { promisify } = require('util');
const bodyparser = require('body-parser');
const path = require('path');

const db = mysql.createConnection({
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE
});

exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;

    if( !email || !password ) {
      return res.status(400).render('login', {
        message: 'Please provide an email and password'
      })
    }

    db.query('SELECT * FROM users WHERE email = ?', [email], async (error, results) => {
      console.log(results);
      if( !results || !(await bcrypt.compare(password, results[0].password)) ) {
        res.status(401).render('login', {
          message: 'Email or Password is incorrect'
        })
      } else {
        const id = results[0].id;

        const token = jwt.sign({ id }, process.env.JWT_SECRET, {
          expiresIn: process.env.JWT_EXPIRES_IN
        });

        console.log("The token is: " + token);

        const cookieOptions = {
          expires: new Date(
            Date.now() + process.env.JWT_COOKIE_EXPIRES * 24 * 60 * 60 * 1000
          ),
          httpOnly: true
        }

        res.cookie('jwt', token, cookieOptions );
        res.status(200).redirect("/");
      }

    })

  } catch (error) {
    console.log(error);
  }
}

exports.register = (req, res) => {
  console.log(req.body);

  const { name, email, password, passwordConfirm } = req.body;

  db.query('SELECT email FROM users WHERE email = ?', [email], async (error, results) => {
    if(error) {
      console.log(error);
    }

    if( results.length > 0 ) {
      return res.render('register', {
        message: 'That email is already in use'
      })
    } else if( password !== passwordConfirm ) {
      return res.render('register', {
        message: 'Passwords do not match'
      });
    } else if( !password || !passwordConfirm ) {
      return res.render('register', {
        message: 'Empty password fields'
      });
    }
    let hashedPassword = await bcrypt.hash(password, 8);
    console.log(hashedPassword);

    const img = 'http://127.0.0.1:5001/default.png'

    db.query('INSERT INTO users SET ?', {name: name, email: email, password: hashedPassword, img: img }, (error, results) => {
      if(error) {
        console.log(error);
      } else {
        console.log(results);
        return res.render('register', {
          message: 'User registered'
        });
      }
    })


  });

}

exports.isLoggedIn = async (req, res, next) => {
  // console.log(req.cookies);
  if( req.cookies.jwt) {
    try {
      //1) verify the token
      const decoded = await promisify(jwt.verify)(req.cookies.jwt,
      process.env.JWT_SECRET
      );

      console.log(decoded);

      //2) Check if the user still exists
      db.query('SELECT * FROM users WHERE id = ?', [decoded.id], (error, result) => {
        console.log(result);

        if(!result) {
          return next();
        }

        req.user = result[0];
        console.log("user is")
        console.log(req.user.email);

        return next();
      });
    } catch (error) {
      console.log(error);
      return next();
    }
  } else {
    next();
  }
}

exports.password = async (req, res, next) => {
  

  const { currentPassword, newPassword } = req.body;
    // console.log(req.cookies);
    if( req.cookies.jwt) {
      try {
        //1) verify the token
        const decoded = await promisify(jwt.verify)(req.cookies.jwt,
        process.env.JWT_SECRET
        );
  
        console.log(decoded);
  
        //2) Check if the user still exists
        db.query('SELECT * FROM users WHERE id = ?', [decoded.id], async (error, result) => {
          console.log(result);
  
          if(!result) {
            return next();
          }
  
          req.user = result[0];
          console.log(req.user.password);

          if(!currentPassword || !newPassword) {
            return res.render('profile', {
              message: 'One of the fields is empty',
              user: req.user
            });
          }

          if(currentPassword === newPassword){
            return res.status(401).render('profile', {
              message: 'Passwords must not be equal',
              user: req.user
            });
          }

          if(!(await bcrypt.compare(currentPassword, req.user.password))){
            return res.status(401).render('profile', {
              message: 'Please enter correct current password',
              user: req.user
            });
          }

          let hashedPassword = await bcrypt.hash(newPassword, 8);
          db.query('UPDATE users SET password = ? WHERE id = ?',[hashedPassword,decoded.id], (error, result) => {
            if(error){
              console.log(error);
            } else {
              console.log(result);
              res.cookie('jwt', 'logout', {
                expires: new Date(Date.now() + 2*1000),
                httpOnly: true
              });
            
              res.status(200).redirect('/');
            }
          });
          
  
        });
      } catch (error) {
        console.log(error);
        return next();
      }
    } else {
      next();
    }
}



exports.image = async (req,res) => {
  if( req.cookies.jwt) {
    try {
      //1) verify the token
      const decoded = await promisify(jwt.verify)(req.cookies.jwt,
      process.env.JWT_SECRET
      );

      console.log(decoded);

      //2) Check if the user still exists
      db.query('SELECT * FROM users WHERE id = ?', [decoded.id], (error, result) => {
        console.log(result);

        if(!result) {
          return next();
        }

        if (!req.file) {
              req.user = result[0];
              console.log(req.user);
              return res.render('profile', {
                user: req.user,
                message2: 'no image to upload'
              });
        }

        req.user = result[0];
        console.log(req.file.filename)
        const imgsrc = 'http://127.0.0.1:5001/' + req.file.filename

        if(result.length > 0) {
          db.query('UPDATE users SET img = ? WHERE email = ?',[imgsrc,req.user.email], (err, result) => {
            if (err) throw err;
            return res.redirect('/profile');
          })
        }
      });
    } catch (error) {
      console.log(error);
      return next();
    }
  } else {
    next();
  }
}

exports.logout = async (req, res) => {
  res.cookie('jwt', 'logout', {
    expires: new Date(Date.now() + 2*1000),
    httpOnly: true
  });

  res.status(200).redirect('/');
}